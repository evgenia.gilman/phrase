<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhraseRequest;
use App\Models\Phrase;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PhrasesController extends Controller
{


    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $phrases = Phrase::all();
        return view('phrases.index', compact('phrases'));
    }


    /**
     * @param PhraseRequest $request
     * @return RedirectResponse
     */
    public function store(PhraseRequest $request)
    {
        $phrase = new Phrase();

        $user = Auth::user();
        $data = [ 'user_id' => $user->id,
            'ru' =>
                [
                    'phrase' => $request->input('ru_phrase'),
                ]
        ];

        $phrase->create($data);

        return back();

    }


    /**
     * @param Phrase $phrase
     * @return Application|Factory|View
     */
    public function show(Phrase $phrase)
    {
        return view('phrases.show', compact('phrase'));
    }

    /**
     * @param Request $request
     * @param Phrase $phrase
     * @return RedirectResponse
     */
    public function update(Request $request, Phrase $phrase)
    {
        if(!empty($request->input('en_phrase'))) {
            $data = [
                'en' =>
                    [
                        'phrase' => $request->input('en_phrase'),
                    ]
            ];
            $phrase->update($data);
        }
            if(!empty($request->input('cs_phrase'))) {
                $data = [
                    'cs' =>
                        [
                            'phrase' => $request->input('cs_phrase'),
                        ]
                ];
                $phrase->update($data);
            }
                if(!empty($request->input('de_phrase'))) {
                    $data = [
                        'de' =>
                            [
                                'phrase' => $request->input('de_phrase'),
                            ]
                    ];
                    $phrase->update($data);
                }
        if(!empty($request->input('ky_phrase'))) {
            $data = [
                'ky' =>
                    [
                        'phrase' => $request->input('ky_phrase'),
                    ]
            ];
            $phrase->update($data);
        }

        return back();
    }
}
