<?php

namespace Database\Factories;

use App\Models\Phrase;
use Illuminate\Database\Eloquent\Factories\Factory;

class PhraseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Phrase::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $ru_faker = \Faker\Factory::create('ru_RU');
        $cs_faker = \Faker\Factory::create('cs_CS');
        $de_faker = \Faker\Factory::create('de_DE');
        $ky_faker = \Faker\Factory::create('ky_KY');
        return [
            'ru' => [
                'phrase' => 'RU - '. $ru_faker->sentence(),
            ],
            'en' => [
                'phrase' => $this->faker->sentence(),
            ],
            'cs' => [
                'phrase' =>  'CS - '.$cs_faker->sentence(),
            ],
            'de' => [
                'phrase' =>  'DE - '.$de_faker->sentence(),
            ],
            'ky' => [
                'phrase' =>'KY - '. $ky_faker->sentence(),
            ],
            'user_id' => rand(1, 5)
        ];
    }
}
