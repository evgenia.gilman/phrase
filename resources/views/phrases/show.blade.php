@extends('layouts.app')
@section('content')
    <div class="container">
        <dv>
            <h3>@lang('messages.can_translate') "{{$phrase->translate('ru')->phrase}}"</h3>
        </dv>


        @if(Auth::check())
            <form method="post" action="{{route('phrases.update', ['phrase' => $phrase])}}">
                @method('put')
                @csrf
                <input type="hidden" id="phrase_id" value="{{$phrase->id}}">
                @if(!empty($phrase->translate('ru')->phrase))
                    <h4>{{$phrase->translate('ru')->phrase}}</h4>
                @else
                    <div class="form-group">
                        <label for="ru_phrase">@lang('messages.russian')</label>
                        <input name="ru_phrase"  type="text" class="form-control"
                               id="ru_phrase">
                    </div>
                @endif
                @if(!empty($phrase->translate('en')->phrase))
                    <h4>{{$phrase->translate('en')->phrase}}</h4>
                @else
                <div class="form-group">
                    <label for="en_phrase">@lang('messages.english')</label>
                    <input name="en_phrase"  type="text" class="form-control"
                           id="en_phrase">
                </div>
                @endif
                @if(!empty($phrase->translate('cs')->phrase))
                    <h4>{{$phrase->translate('cs')->phrase}}</h4>
                @else
                <div class="form-group">
                    <label for="cs_phrase">@lang('messages.czech')</label>
                    <input name="cs_phrase" type="text" class="form-control"
                           id="cs_phrase">
                </div>
                @endif
                @if(!empty($phrase->translate('de')->phrase))
                    <h4>{{$phrase->translate('de')->phrase}}</h4>
                @else
                <div class="form-group">
                    <label for="de_phrase">@lang('messages.german')</label>
                    <input name="de_phrase" type="text" class="form-control"
                           id="de_phrase" >
                </div>
                @endif
                @if(!empty($phrase->translate('ky')->phrase))
                    <h4>{{$phrase->translate('ky')->phrase}}</h4>
                @else
                <div class="form-group">
                    <label for="ky_phrase">@lang('messages.kyrgyz')</label>
                    <input name="ky_phrase" type="text" class="form-control"
                           id="ky_phrase" >
                </div>
                @endif
                <button type="submit" class="btn btn-primary">@lang('messages.save_phrase')</button>
            </form>
        @else
                @if(!empty($phrase->translate('ru')->phrase))
                    <div>
                        <b>@lang('messages.translate_ru')</b><br>
                        <h5>
                            {{$phrase->translate('ru')->phrase}}
                        </h5>
                    </div>
                @endif

                @if(!empty($phrase->translate('en')->phrase))
                    <div>
                        <b>@lang('messages.translate_en')</b><br>
                        <h5>
                            {{$phrase->translate('en')->phrase}}
                        </h5>
                    </div>
                @endif

                @if(!empty($phrase->translate('cs')->phrase))
                    <div>
                        <b>@lang('messages.translate_cs')</b><br>
                        <h5>
                            {{$phrase->translate('cs')->phrase}}
                        </h5>
                    </div>
                    @endif
                    @if(!empty($phrase->translate('de')->phrase))
                        <div>
                            <b>@lang('messages.translate_de')</b><br>
                            <h5>
                                {{$phrase->translate('de')->phrase}}
                            </h5>
                        </div>
                    @endif
                    @if(!empty($phrase->translate('ky')->phrase))
                        <div>
                            <b>@lang('messages.translate_ky')</b><br>
                            <h5>
                                {{$phrase->translate('ky')->phrase}}
                            </h5>
                        </div>
                    @endif
        @endif
    </div>

    @endsection
