<?php

return [
    'phrases' => 'Phrases',
    'create_new_phrase' => 'Create new phrase',
    'save_phrase' => 'Save phrase',
    'phrase' => 'Phrase',
    'russian' => 'Translate into Russian',
    'english' => 'Translate into English',
    'czech' => 'Translate into Czech',
    'german'  => 'Translate into German',
    'kyrgyz' => 'Translate into Kyrgyz',
    'translate_ru' => 'This phrase has been translated into Russian',
    'translate_en' => 'This phrase has been translated into English',
    'translate_cs' => 'This phrase has been translated into Czech',
    'translate_de' => 'This phrase has been translated into German',
    'translate_ky' => 'This phrase has been translated into Kyrgyz',
    'login' => 'Login',
    'register' => 'Register',
    'can_translate' => 'Please translate'
];
